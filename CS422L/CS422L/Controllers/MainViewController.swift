//
//  ViewController.swift
//  CS422L
//
//  Created by Jonathan Sligh on 1/29/21.
//

import UIKit
import CoreData

class MainViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource  {

    @IBOutlet var collectionView: UICollectionView!
    var sets: [FlashcardSets]?
    
    //Reference to managed object contetext
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if (UserDefaults.standard.bool(forKey: "setIsDownloaded")) {
            print("data already downloaded")
        }
        else{
            UserDefaults.standard.set(true, forKey: "setIsDownloaded")
            let firstFlashcardset = FlashcardSets(context: self.context)
            firstFlashcardset.title = "Title \(1)"
            do{
                try self.context.save()
            }
            catch{
            }
        }
        self.fetchFlashcardSets()
        collectionView.delegate = self
        collectionView.dataSource = self
        makeThingsLookPretty()
    }
    
    
    @IBAction func addNewSet(_ sender: Any) {
        let newSet = FlashcardSets(context: self.context)
        newSet.title = "Title \(sets!.count + 1)"
        //sets.append(newSet)
        do{
            try self.context.save()
        }
        catch{
        }
        self.fetchFlashcardSets()
        collectionView.reloadData()
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.sets?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SetCell", for: indexPath) as! FlashcardSetCollectionCell
        //Get Flashcardsets from array and set cell
        let FlashcardSets = self.sets![indexPath.row]
        
        cell.textLabel?.text = FlashcardSets.title
        
        //setup method just makes it look nice
        cell.setup()
        //cell.textLabel.text = sets[indexPath.row].title
        return cell
    }
    

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //go to new view
        getVC(set: self.sets![indexPath.row])
        performSegue(withIdentifier: "GoToDetail", sender: self)
    }
    
    
    //another function to make things look nice
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let noOfCellsInRow = 2   //number of column you want
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(noOfCellsInRow - 1))

        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(noOfCellsInRow))
        return CGSize(width: size, height: size)
    }
    
    
    func getVC(set: FlashcardSets!){
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let currentSet = sb.instantiateViewController(withIdentifier: "DetailVC") as! FlashCardSetDetailViewController
        currentSet.parentingVC = self
        currentSet.currentSet = set
        currentSet.modalPresentationStyle = .overCurrentContext
        self.present(currentSet, animated: false, completion: nil)
    }
    
    //Fetch the data from core data to display
    func fetchFlashcardSets(){
        do {
            self.sets = try context.fetch(FlashcardSets.fetchRequest())
            
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
        catch{
        }
    }
    
    
    //just a function to make things look nice
    func makeThingsLookPretty()
    {
        let margin: CGFloat = 10
        guard let collectionView = collectionView, let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout else { return }
        flowLayout.minimumInteritemSpacing = margin
        flowLayout.minimumLineSpacing = margin
        flowLayout.sectionInset = UIEdgeInsets(top: margin, left: margin, bottom: margin, right: margin)
    }
    
    
}

