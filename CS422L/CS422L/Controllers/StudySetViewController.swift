//
//  StudySetViewController.swift
//  CS422L
//
//  Created by Jonathan Sligh on 2/18/21.
//

import UIKit
import CoreData

class StudySetViewController: UIViewController {

    @IBOutlet var cardView: UIView!
    @IBOutlet var completedLabel: UILabel!
    @IBOutlet var termLabel: UILabel!
    @IBOutlet var missedButton: UIButton!
    @IBOutlet var skipButton: UIButton!
    @IBOutlet var correctButton: UIButton!
    @IBOutlet var missedLabel: UILabel!
    @IBOutlet var correctLabel: UILabel!
    //var flashcards: [Flashcard] = []
    var theFlashcards: [Flashcards]?
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    var currentIndex = 0
    var missedCount = 0
    var correctCount = 0
    var completedCount = 0
    var totalCount = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setup()
    }
    
    
    func setup()
    {
        //flashcards = Flashcard.getHardCodedCollection()
        fetchFlashcards()
        //totalCount = getRecordsCount()
        totalCount = theFlashcards!.count
        reloadViews()
        let gesture = UITapGestureRecognizer(target: self, action: #selector(showDefinition(_:)))
        cardView.addGestureRecognizer(gesture)
        makeItPretty()
    }
    
    
    @IBAction func showDefinition(_ sender:UITapGestureRecognizer)
    {
        //reset everything if at the end
        if theFlashcards!.count == 0
        {
            //flashcards = Flashcard.getHardCodedCollection()
            fetchFlashcards()
            currentIndex = 0
            missedCount = 0
            correctCount = 0
            completedCount = 0
            totalCount = getRecordsCount()
            reloadViews()
            return
        }
        termLabel.text = self.theFlashcards![currentIndex].definition
    }
    
    
    @IBAction func missedClick(_ sender: Any) {
        missedCount += 1
        theFlashcards![currentIndex].missed = true
        currentIndex += 1
        if currentIndex > theFlashcards!.count - 1
        {
            currentIndex = 0
        }
        reloadViews()
    }
    
    
    @IBAction func skipClick(_ sender: Any) {
        currentIndex += 1
        if currentIndex > theFlashcards!.count - 1
        {
            currentIndex = 0
        }
        reloadViews()
    }
    
    
    @IBAction func correctClick(_ sender: Any) {
        if theFlashcards!.count == 0
        {
            termLabel.text = "Click here to reset!"
            return
        }
        if !theFlashcards![currentIndex].missed
        {
            correctCount += 1
        }
        completedCount += 1
        theFlashcards!.remove(at: currentIndex)
        if currentIndex > theFlashcards!.count - 1
        {
            currentIndex = 0
        }
        reloadViews()
    }
    
    
    func getRecordsCount() -> Int{
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Flashcards")
        do{
            let count = try context.count(for: fetchRequest)
            return count
        }
        catch{
            print(error.localizedDescription)
        }
        return totalCount
    }
    
    
    func fetchFlashcards(){
        do{
            self.theFlashcards = try context.fetch(Flashcards.fetchRequest())
            DispatchQueue.main.async {
                self.termLabel.reloadInputViews()
            }
        }
        catch{
        }
    }
    
    
    func reloadViews()
    {
        completedLabel.text = "Completed: \(completedCount) / \(totalCount)"
        missedLabel.text = "Missed: \(missedCount)"
        correctLabel.text = "Correct: \(correctCount)"
        if theFlashcards!.count == 0
        {
            termLabel.text = "Click here to reset!"
            return
        }
        termLabel.text = self.theFlashcards![currentIndex].term
    }
    
    
    func makeItPretty()
    {
        cardView.layer.cornerRadius = 20.0
        cardView.layer.borderWidth = 3.0
        cardView.layer.borderColor = UIColor.systemPurple.cgColor
        missedButton.layer.cornerRadius = 8.0
        missedButton.layer.borderWidth = 1.0
        missedButton.layer.borderColor = UIColor.systemRed.cgColor
        skipButton.layer.cornerRadius = 8.0
        skipButton.layer.borderWidth = 1.0
        skipButton.layer.borderColor = UIColor.systemPurple.cgColor
        correctButton.layer.cornerRadius = 8.0
        correctButton.layer.borderWidth = 1.0
        correctButton.layer.borderColor = UIColor.systemBlue.cgColor
    }
    
}
