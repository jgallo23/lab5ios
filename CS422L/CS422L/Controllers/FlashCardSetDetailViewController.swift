//
//  FlashCardSetDetailActivity.swift
//  CS422L
//
//  Created by Jonathan Sligh on 2/3/21.
//

import Foundation
import UIKit
import CoreData

class FlashCardSetDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate
{
    var cards: [Flashcards]?
    var currentSet: FlashcardSets?
    var parentingVC: MainViewController?
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    @IBOutlet var buttonView: UIView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var deleteButton: UIButton!
    @IBOutlet var studyButton: UIButton!
    @IBOutlet var addButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if (UserDefaults.standard.bool(forKey: "cardsIsDownloaded")){
            print("Cards are already downloaded")
        }
        else{
            UserDefaults.standard.set(true, forKey: "cardsIsDownloaded")
            let firstCard = Flashcards(context: self.context)
            firstCard.definition = "Definition \(1)"
            firstCard.term = "Term \(1)"
            do{
                try self.context.save()
            }
            catch{
            }
        }
        self.fetchFlashcards()
        //cards = Flashcard.getHardCodedCollection()
        tableView.delegate = self
        tableView.dataSource = self
        let longPressedGesture = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress(gestureRecognizer:)))
        longPressedGesture.minimumPressDuration = 0.5
        longPressedGesture.delegate = self
        longPressedGesture.delaysTouchesBegan = true
        tableView.addGestureRecognizer(longPressedGesture)
        makeItPretty()
    }
    
    
    //adds card
    @IBAction func addCard(_ sender: Any) {
        let newCard = Flashcards(context: self.context)
        newCard.term = "Term \(cards!.count + 1)"
        newCard.definition = "Definition \(cards!.count + 1)"
        do{
            try self.context.save()
        }
        catch{
        }
        fetchFlashcards()
        tableView.reloadData()
    }
    
    
    @IBAction func deleteCard(_ sender: Any){
        //_ = navigationController?.popViewController(animated: true)
        context.delete(currentSet!)
        parentingVC?.fetchFlashcardSets()
        self.dismiss(animated: true)
        //_ = navigationController?.popViewController(animated: true)
    }
    
    
    @objc func handleLongPress(gestureRecognizer: UILongPressGestureRecognizer) {
        if (gestureRecognizer.state != .began) {
                return
            }

        let p = gestureRecognizer.location(in: tableView)
        if let indexPath = tableView.indexPathForRow(at: p) {
            createCustomAlert(card: cards![indexPath.row])
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cards!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CardCell", for: indexPath) as! FlashcardTableViewCell
        cell.flashcardLabel.text = cards![indexPath.row].term
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let alert = UIAlertController(title: cards![indexPath.row].term, message: cards![indexPath.row].definition, preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "Edit", style: .default, handler: {_ in
            self.createCustomAlert(card: self.cards![indexPath.row])
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
    
    func createCustomAlert(card: Flashcards!)
    {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let alertVC = sb.instantiateViewController(identifier: "EditAlertViewController") as! EditAlertViewController
        alertVC.parentVC = self
        alertVC.card = card
        alertVC.modalPresentationStyle = .overCurrentContext
        self.present(alertVC, animated: false, completion: nil)
    }
    
    
    //Fetch the data from core data to display
    func fetchFlashcards(){
        do {
            self.cards = try context.fetch(Flashcards.fetchRequest())
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
        catch{
        }
    }
    
    //just a function to make everything look nice
    func makeItPretty()
    {
        buttonView.layer.cornerRadius = 8.0
        buttonView.layer.borderColor = UIColor.purple.cgColor
        buttonView.layer.borderWidth = 2.0
        deleteButton.layer.cornerRadius = 8.0
        studyButton.layer.cornerRadius = 8.0
        addButton.layer.cornerRadius = 8.0
    }
}
